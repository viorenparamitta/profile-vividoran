from django.shortcuts import render, redirect
from .forms import FormKegiatan, FormUser
from .models import Kegiatan, User

def addActivity(request):
    formKegiatan = FormKegiatan()
    if request.method == "POST":
        formKegiatan_input = FormKegiatan(request.POST)
        if formKegiatan_input.is_valid():
            data = formKegiatan_input.cleaned_data
            kegiatan_input = Kegiatan()
            kegiatan_input.namaKegiatan = data['namaKegiatan']
            kegiatan_input.desKegiatan = data['desKegiatan']
            kegiatan_input.save()
            return render(request, 'addAct.html', {'form': formKegiatan, 'status': 'failed'})
    return render(request, 'addAct.html', {'form': formKegiatan})


def listActivity(request):
    kegiatan = Kegiatan.objects.all()
    return render(request, 'listAct.html', {'kegiatan': kegiatan})

def register(request, task_id):
    formUser = FormUser()
    if request.method == "POST":
        formUser_input = FormUser(request.POST)
        if formUser_input.is_valid():
            data = formUser_input.cleaned_data
            orangBaru = User()
            orangBaru.namaUser = data['namaUser']
            orangBaru.kegiatan = Kegiatan.objects.get(id=task_id)
            orangBaru.save()
            return redirect('/planner/listActivity')
        return render(request, 'regis.html', {'form': formUser, 'status': 'failed'})
    return render(request, 'regis.html', {'form': formUser})

