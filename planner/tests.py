from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .models import Kegiatan, User
from .views import addActivity, listActivity, register
from .forms import FormKegiatan, FormUser
from .apps import PlannerConfig


class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            namaKegiatan="liburan", desKegiatan="liburan ke pantai")
        self.user = User.objects.create(namaUser="vioren")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(User.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "liburan")
        self.assertEqual(str(self.user), "vioren")


class FormTest(TestCase):

    def test_form_is_valid(self):
        formKegiatan = FormKegiatan(data={"namaKegiatan": "liburan","desKegiatan": "liburan ke pantai"})
        self.assertTrue(formKegiatan.is_valid())
        formUser = FormUser(data={'namaUser': "vioren"})
        self.assertTrue(formUser.is_valid())

    def test_form_invalid(self):
        formKegiatan = FormKegiatan(data={})
        self.assertFalse(formKegiatan.is_valid())
        formUser = FormUser(data={})
        self.assertFalse(formUser.is_valid())


class UrlsTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(namaKegiatan = "liburan", desKegiatan = "liburan ke pantai")
        self.user = User.objects.create(namaUser = "ojan", kegiatan=Kegiatan.objects.get(namaKegiatan = "liburan"))
        self.listActivity = reverse("listActivity")
        self.addActivity = reverse("addActivity")
        self.register = reverse("register", args = [self.kegiatan.pk])

    def test_listActivity_use_right_function(self):
        a = resolve(self.listActivity)
        self.assertEqual(a.func, listActivity)

    def test_addActivity_use_right_function(self):
        a = resolve(self.addActivity)
        self.assertEqual(a.func, addActivity)

    def test_register_use_right_function(self):
        a = resolve(self.register)
        self.assertEqual(a.func, register)


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.listActivity = reverse("listActivity")
        self.addActivity = reverse("addActivity")

    def test_GET_listActivity(self):
        response = self.client.get(self.listActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listAct.html')

    def test_GET_addActivity(self):
        response = self.client.get(self.addActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addAct.html')

    def test_POST_addActivity(self):
        response = self.client.post(self.addActivity,{'namaKegiatan': 'liburan', 'desKegiatan ': "belajar PPW"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addActivity_invalid(self):
        response = self.client.post(self.addActivity,{'namaKegiatan': '', 'desKegiatan ': ""}, follow = True)
        self.assertTemplateUsed(response, 'addAct.html')


class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(namaKegiatan = "belajar", desKegiatan = "belajar SDA")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/planner/register/1/', data={'namaUser': 'saha ya'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/planner/register/1/')
        self.assertTemplateUsed(response, 'regis.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/planner/register/1/', data={'nama': ''})
        self.assertTemplateUsed(response, 'regis.html')


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(PlannerConfig.name, 'planner')
        self.assertEqual(apps.get_app_config('planner').name, 'planner')
