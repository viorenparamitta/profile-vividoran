from django.urls import path, include
from .views import addActivity, listActivity, register
from .models import Kegiatan, User

urlpatterns = [
    path('addActivity/', addActivity, name='addActivity'),
    path('listActivity/', listActivity, name='listActivity'),
    path('register/<int:task_id>/', register, name='register'),
]