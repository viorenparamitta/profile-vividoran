from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=50)
    desKegiatan = models.CharField(max_length=500)

    def listPeserta(self):
        return User.objects.filter(kegiatan=self.id)

    peserta = property(listPeserta)

    def __str__(self):
        return self.namaKegiatan

class User(models.Model):
    namaUser = models.CharField(max_length=50)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null=True, blank=True)
    def __str__(self):
        return self.namaUser
