from django import forms
from .models import Kegiatan, User

class FormKegiatan(forms.Form):
    namaKegiatan = forms.CharField(
        label = "nama kegiatan",
        max_length = 50,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control w-50',
            }
        )
    )
    desKegiatan = forms.CharField(
        label = "deskripsi kegiatan",
        max_length = 500,
        widget = forms.Textarea(
        )
    )

class FormUser(forms.Form):
    namaUser = forms.CharField(
        label = "isi nama kamu",
        max_length = 50,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control w-50',
            }
        )
    )
