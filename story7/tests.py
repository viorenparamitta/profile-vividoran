from django.test import TestCase, Client
from django.urls import resolve
from .views import summary
from .apps import Story7Config

# Create your tests here.

class UnitTest(TestCase):
    def test_url_summary_is_exist(self):
        response = Client().get('/summary/')
        self.assertEqual(response.status_code, 200)
    
    def test_story7_using_summary_template(self):
        response = Client().get('/summary/')
        self.assertTemplateUsed(response, 'summary.html')
    
    # def test_story7_using_summary(self):
    #     found = resolve('/summary/')
    #     self.assertEqual(found.func, summary)
    
    # def test_app(self):
    #     self.assertEqual(Story7Config.name, 'story7')
