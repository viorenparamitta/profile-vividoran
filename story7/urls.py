from django.urls import path, include
from .views import summary

urlpatterns = [
    path('', summary, name='summary'),
]