from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def search(request):
    response = {}
    return render(request, 'search.html', response)

def data(request):
    input = request.GET['input']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + input
    response = requests.get(url)
    json_read = response.json()
    return JsonResponse(json_read)
