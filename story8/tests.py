from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import search, data
from .apps import Story8Config
# # Create your tests here.

class UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get("/search/")
        self.assertEqual(response.status_code,200)

    def test_story8_search_func(self):
        found = resolve("/search/")
        self.assertEqual(found.func, search)
    
    def test_using_summary_template(self):
        response = Client().get("/search/")
        self.assertTemplateUsed(response,'search.html')
    
    def test_fungsi_cari_jalan(self):
        response_get = Client().get("/search/data", {"input": "a"})
        self.assertEqual(response_get.status_code, 200)

    def test_app(self):
        self.assertEqual(Story8Config.name, 'story8')
