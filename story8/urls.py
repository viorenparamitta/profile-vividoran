from django.urls import path, include
from .views import search, data

urlpatterns = [
    path('', search, name='search'),
    path('data', data, name='data')
]