from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from django.apps import apps
from .apps import Story9Config
from .forms import LoginForm, RegisterForm

class UnitTest(TestCase):
    def test_homepage_url_exists(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_exists(self):
        response = Client().get('/story9/signup/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_exists(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exists(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

    def test_create_model(self):
        User.objects.create_user(
            username="vioren", email="vioren@gmail.com", password="vvdrn")
        self.assertEqual(User.objects.all().count(), 1)

    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
    
    def test_forms_register_valid(self):
        form_reg = RegisterForm(data={
            "username": "hahaha",
            "email": "hahaha@gmail.com",
            'password1': '345345345',
            'password2': '345345345',
        })
        self.assertTrue(form_reg.is_valid())

    def test_forms_register_invalid(self):
        form_regs = RegisterForm(data={
            "username": "",
            "email": "hahaha@gmail.com",
            'password1': '345345345',
            'password2': '345345345',
        })
        self.assertFalse(form_regs.is_valid())

    def test_forms_login_valid(self):
        form_login = LoginForm(data={
            "username": "haihai",
            "password": "hihihihi"
        })
        self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = LoginForm(data={
            "username": "",
            "password": ""
        })
        self.assertFalse(form_login.is_valid())


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login = reverse("login")
        self.regs = reverse("signup")

    def test_POST_reg_valid(self):
        response = self.client.post(self.regs,
                                    {
                                        "username": "hahaha",
                                        "email": "hahaha@gmail.com",
                                        'password1': 'hahahaha',
                                        'password2': 'hahahaha',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_reg_invalid(self):
        response = self.client.post(self.regs,
                                    {
                                        "username": "hahaha",
                                        "email": "hahaha@gmail.com",
                                        'password1': 'hahahaha',
                                        'password2': '',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'signup.html')

    def test_POST_login_valid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': "hahaha",
                                        'password ': "hahahaha"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_login_invalid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': "",
                                        'password ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'login.html')

