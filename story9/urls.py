from django.urls import path
from .views import index, signUp, log_in, log_out
from .models import User

urlpatterns = [
    path('', index, name='index'),
    path('signup/', signUp, name='signup'),
    path('login/', log_in, name='login'),
    path('logout/', log_out, name='logout')
]
