from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, reverse
from .forms import RegisterForm, LoginForm
from .models import User


def index(request):
    return render(request, 'indexs9.html')


def signUp(request):
    formSignUp = RegisterForm()
    if request.method == 'POST':
        formSignUp_input = RegisterForm(request.POST)
        if formSignUp_input.is_valid():
            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password1']
            if User.objects.filter(username=username).exists():
                messages.error(request, 'username is already taken!')
            else:
                user = User.objects.create_user(username, email, password)
                return redirect('/story9/login')
    return render(request, 'signup.html', {'form': formSignUp})


def log_in(request):
    formLogin = LoginForm()
    if request.method == 'POST':
        formLogin_input = LoginForm(request.POST)
        if formLogin_input.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            if User.objects.filter(username=username).exists():
                user = authenticate(
                    request, username=username, password=password)
                if user is not None:
                    login(request, user)
                    return redirect('/story9/')
                else:
                    messages.error(
                        request, 'your password is invalid, please try again')
            else:
                messages.error(
                    request, 'your username is invalid, please try again')
    return render(request, 'login.html', {'form': formLogin})


def log_out(request):
    logout(request)
    return redirect('/story9/')
