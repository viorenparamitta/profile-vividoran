from django import forms
from .models import User
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class RegisterForm(forms.Form):
    username = forms.CharField(label='Username', widget=forms.TextInput(
        attrs={
            'class': 'form-control form-custom',
            'placeholder': 'Username'
        }
    ))

    email = forms.CharField(label='Email', widget=forms.EmailInput(
        attrs={
            'class': 'form-control form-custom',
            'placeholder': 'Email'
        }
    ))

    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control form-custom',
            'placeholder': 'Password'
        }
    ))

    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control form-custom',
            'placeholder': 'Confirm Password'
        }
    ))

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control form-custom',
            'placeholder': 'Username'
        }
    ))

    password = forms.CharField(min_length=8, widget=forms.PasswordInput(
        attrs={
            'class': 'form-control form-custom',
            'placeholder': 'Password'
        }
    ))
