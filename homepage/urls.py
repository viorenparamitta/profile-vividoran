from django.urls import path, include
from .views import base
from .views import story3
from .views import proto
from .views import story1
from .views import photos
from .views import poetry

urlpatterns = [
    path('', base),
    path('story3/', story3),
    path('story3-porto/', proto),
    path('story1/', story1),
    path('story1-photos/', photos),
    path('story1-poetry/', poetry),
]