from django.shortcuts import render

# Create your views here.
def base(request):
    return render(request, "aiueo.html")

def story3(request):
    return render(request, "index.html")

def proto(request):
    return render(request, "proto.html")

def story1(request):
    return render(request, "index_lab1.html")

def photos(request):
    return render(request, "photos.html")

def poetry(request):
    return render(request, "poetry.html")