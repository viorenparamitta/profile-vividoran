from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import Form_Matkul
from .models import FormMatkul

# Create your views here.
def form(request):
    if request.method == "POST":
        forms = Form_Matkul(request.POST)
        if forms.is_valid():
            submitted_data = FormMatkul()
            submitted_data.MataKuliah = forms.cleaned_data['MataKuliah']
            submitted_data.Dosen = forms.cleaned_data['Dosen']
            submitted_data.JumlahSKS = forms.cleaned_data['JumlahSKS']
            submitted_data.Ruangan = forms.cleaned_data['Ruangan']
            submitted_data.Semester = forms.cleaned_data['Semester']
            submitted_data.Deskripsi = forms.cleaned_data['Deskripsi']
            submitted_data.save()
            result = 1
        else:
            result = 0
        statusMatkul = FormMatkul.objects.all()
        return render(request, "form.html", {"forms": forms, "database":statusMatkul,"result":result})
    else:
        forms = Form_Matkul()
        statusMatkul = FormMatkul.objects.all()
        return render(request, "form.html", {"forms": forms, "database":statusMatkul})

def jadwal(request):
    DaftarMatkul = FormMatkul.objects.all()
    return render(request, "jadwal.html", {"DaftarMatkul": DaftarMatkul})

def detail(request,id):
    DaftarMatkul = FormMatkul.objects.get(pk = id)
    return render(request, "detail.html", {"DaftarMatkul": DaftarMatkul})

def delete(request, id_matkul):
    DaftarMatkul = FormMatkul.objects.all()
    if(request.method == 'POST'):
        FormMatkul.objects.filter(pk = id_matkul).delete()
        return render(request, "jadwal.html", {"DaftarMatkul": DaftarMatkul, "status": 'Success'})
    else:
        return render(request, "jadwal.html", {"DaftarMatkul": DaftarMatkul, "status": 'Failed'})
