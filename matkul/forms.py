from django import forms
from .models import FormMatkul

class Form_Matkul(forms.Form):
    class Meta:
      model = FormMatkul
      fields = '__all__'

    MataKuliah = forms.CharField(
        label = "Nama mata kuliah", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )

    Dosen = forms.CharField(
        label = "Nama dosen", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )

    JumlahSKS = forms.IntegerField(
        label = "Jumlah SKS",
        widget = forms.NumberInput(
            attrs={
                'class':'form-control',
                'min':'1',
                'max':'4'
            }
        )
    )

    Ruangan = forms.CharField(
        label = "Ruangan", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
            }
        )
    )

    Semester = forms.CharField(
        label = "Semester",
        widget = forms.TextInput(
            attrs={
                'class' : 'form-control',
            }
        )   
    )


    Deskripsi = forms.CharField(
        label = "Deskripsi mata kuliah", 
        max_length = 100,
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
            }
        )
    )
