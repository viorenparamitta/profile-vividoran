from django.urls import path, include
from .views import detail, form, jadwal, delete
from multiprocessing.connection import Client
from matkul.models import FormMatkul

urlpatterns = [
    path('form/', form),
    path('jadwal/', jadwal),
    path('<int:id>/', detail, name='detail'),
    path('delete/<id_matkul>/', delete, name='delete')
]
