from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class FormMatkul(models.Model):
    smt_list = [
        ("Gasal 2019/2020","Gasal 2019/2020"),
        ("Genap 2019/2020","Genap 2019/2020"),
        ("Gasal 2020/2021","Gasal 2020/2021"),
        ("Genap 2020/2021","Genap 2020/2021"),
        ("Others","Others")
    ]
    MataKuliah = models.CharField("Mata Kuliah ", max_length = 50, null = True)
    Dosen = models.CharField("Nama Dosen ", max_length = 50, null = True)           
    JumlahSKS = models.IntegerField("Jumlah SKS ", null = True, validators=[MaxValueValidator(24),MinValueValidator(1)])
    Ruangan = models.CharField("Ruang Kelas ", max_length = 50, null = True)
    Semester = models.CharField("Semester ", max_length = 120, choices = smt_list, null = True, default = ("Gasal 2019/2020","Gasal 2019/2020"))
    Deskripsi = models.CharField("Deskripsi Mata Kuliah ", max_length=120, null=True, default="-")

    def __str__(self):
        return f'{self.MataKuliah}'

