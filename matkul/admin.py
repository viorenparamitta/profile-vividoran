from django.contrib import admin
from .models import FormMatkul
# Register your models here.

admin.site.register(FormMatkul)

class adminMatkul(admin.ModelAdmin):
    list_display = ('MataKuliah', 'JumlahSKS', 'Dosen', 'Semester', 'Ruangan', 'Deskripsi')
    search_fields = ('MataKuliah', 'JumlahSKS', 'Dosen', 'Semester', 'Ruangan', 'Deskripsi')